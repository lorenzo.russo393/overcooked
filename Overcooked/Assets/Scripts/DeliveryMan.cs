using Lorenzo;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class DeliveryMan : MonoBehaviour
{
    BTController bt;
    GameManager gm;

    [SerializeField] Transform waitOrder;
    [SerializeField] Transform deliveryOrder;

    [SerializeField] float time = 0;

    NavMeshAgent agent;

    private void Start()
    {
        gm = FindAnyObjectByType<GameManager>();
        agent= GetComponent<NavMeshAgent>();
        bt = FindAnyObjectByType<BTController>();
    }

    private void Update()
    {
        TakeOrder();
    }

    void TakeOrder()
    {
        if (gm.day)
        {
            if (!bt.orderDelivered)
            {
                agent.SetDestination(waitOrder.position);
            }
            else
            {
                agent.SetDestination(deliveryOrder.position);

                if (Vector3.Distance(agent.destination, transform.position) < 0.8f)
                {
                    time += Time.deltaTime;

                    if (time > 1f)
                    {
                        bt.orderDelivered = false;
                        time = 0;
                    }
                }
            }
        }
        else
        {
            agent.SetDestination(deliveryOrder.position);
        }
    }
}
