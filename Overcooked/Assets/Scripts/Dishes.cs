using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Dishes : ScriptableObject
{
    public int bread= 5;
    public int salad = 5;
    public int tomato = 5;
    public int chicken = 5; 
}
