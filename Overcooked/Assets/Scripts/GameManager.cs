using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] Light dirLight;
    public AudioSource orderArrivedSound;

    [Header("Current ingredients")]
    public int currentChicken = 5;
    public int currentBread = 5;
    public int currentSalad = 5;
    public int currentTomato = 5;

    [Header("Current dishes")]
    public int currentChickenBurger = 0;
    public int currentChickenSalad = 0;
    public int currentTomatoSalad = 0;

    [Header("Texts ingredients")]
    [SerializeField] TextMeshProUGUI breadText;
    [SerializeField] TextMeshProUGUI chickenText;
    [SerializeField] TextMeshProUGUI saladText;
    [SerializeField] TextMeshProUGUI tomatoText;

    [Header("Other Texts")]
    public TextMeshProUGUI orderText;
    [SerializeField] TextMeshProUGUI daysText;
    [SerializeField] TextMeshProUGUI speedPlayText;

    [Header("Timers")]
    public float waiUntilStartDay = 0;
    [SerializeField] float dayTime = 0;
    public float nightTime = 0;

    [Header("Dishes bools")]
    public bool chickenBurger = false;
    public bool chickenSalad = false;
    public bool tomatoSalad = false;
    public bool orderArrived = false;
    public bool sendOrder = false;
    public bool canDoOrder = true;

    [SerializeField] float time = 0;
    [SerializeField] float randomNumber = 0;

    public bool day = true;
    [SerializeField] bool canIncreseDay = true;
    [SerializeField] int dayCount = 0;

    [SerializeField] bool restartingDay = false;

    float r = 0;
    float g = 0;
    float b = 1;

    private void Start()
    {
        speedPlayText.text = "1x";

        r = dirLight.color.r;
        g = dirLight.color.g;
        b = dirLight.color.b;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            VelocityUpgrade();
        }
        DayCycle();
        OrdinationsCycle();

        breadText.text = "Bread: " + currentBread;
        chickenText.text = "Chicken: " + currentChicken;
        saladText.text = "Salad: " + currentSalad;
        tomatoText.text = "Tomato: " + currentTomato;
    }

    void OrdinationsCycle()
    {
        if (day && !chickenBurger && !chickenSalad && !tomatoSalad && canDoOrder)
        {
            time += Time.deltaTime;

            if (time >= 4)
            {
                randomNumber = Random.Range(1, 4);

                switch (randomNumber)
                {
                    case 1:
                        orderText.text = "Chicken Burger";
                        orderArrivedSound.Play();
                        chickenBurger = true;
                        orderArrived = true;
                        time = 0;
                        break;
                    case 2:
                        orderText.text = "Chicken Salad";
                        orderArrivedSound.Play();
                        chickenSalad = true;
                        orderArrived = true;
                        time = 0;
                        break;
                    case 3:
                        orderText.text = "Tomato Salad";
                        orderArrivedSound.Play();
                        tomatoSalad = true;
                        orderArrived = true;
                        time = 0;
                        break;
                }
            }
        }
    }

    void DayCycle()
    {
        waiUntilStartDay += Time.deltaTime;

        daysText.text = "Day " + dayCount;

        if (canIncreseDay)
        {
            dayCount++;
            canIncreseDay = false;
            restartingDay = false;
        }

        if (waiUntilStartDay >= 3)
        {
            waiUntilStartDay = 3;

            if (dayTime < 100)
            {
                day = true;
                dayTime += Time.deltaTime;
            }

            if (dayTime >= 100)
            {
                if (r >= 0.1509434 && g >= 0.1446396 && b >= 0.1274475)
                {
                    b -= Time.deltaTime * 0.7f;
                    r -= Time.deltaTime * 0.7f;
                    g -= Time.deltaTime * 0.7f;
                }

                dirLight.color = new Color(r, g, b);

                canDoOrder = false;

                if (!orderArrived)
                {
                    day = false;
                    nightTime += Time.deltaTime;

                    if (nightTime >= 10)
                    {
                        if (r < 1 && g < 0.9568627f && b < 0.8392157f)
                        {
                            b = 0.8392157f;
                            r = 1;
                            g = 0.9568627f;
                        }

                        dirLight.color = new Color(r, g, b);

                        canDoOrder = true;
                        canIncreseDay = true;
                        nightTime = 0;
                        dayTime = 0;
                        waiUntilStartDay = 0;
                    }
                }
            }
        }
    }

    void VelocityUpgrade()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 2;
            speedPlayText.text = "2x";
        }
        else if (Time.timeScale == 2)
        {
            Time.timeScale = 4;
            speedPlayText.text = "4x";
        }
        else if (Time.timeScale == 4)
        {
            Time.timeScale = 6;
            speedPlayText.text = "6x";
        }
        else if (Time.timeScale == 6)
        {
            Time.timeScale = 1;
            speedPlayText.text = "1x";
        }
    }
}
