using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.PlayerLoop;
namespace Lorenzo
{
    public class BTController : MonoBehaviour
    {
        GameManager gm;

        [SerializeField] Animator animPoke;

        [Header("Restock tables")]
        [SerializeField] Transform[] restockTables;

        [Header("Animator")]
        [SerializeField] Animator anim;

        [Header("Restock waypoints")]
        [SerializeField] Transform[] restockWaypoints;

        [Header("Other waypoints")]
        [SerializeField] Transform workTable;
        [SerializeField] Transform deliveryPos;
        [SerializeField] Transform waitPos;
        [SerializeField] Transform bedPos;

        [SerializeField] bool saladTaked = false;

        [SerializeField] float time = 0;
        [SerializeField] bool dishDelivered = false;

        [SerializeField] Dishes[] dishes;

        [SerializeField] bool canAddDish = true;
        [SerializeField] bool canDecreseDish = true;
        [SerializeField] bool checkIngredients = true;
        [SerializeField] bool rotate = false;
        [SerializeField] bool open = false;
        [SerializeField] bool wakeup = false;
        public bool orderDelivered = false;

        BTNode.Status treeStatus = BTNode.Status.Running;

        BTRoot tree;
        NavMeshAgent agent;

        private void Start()
        {
            gm = FindAnyObjectByType<GameManager>();
            agent = GetComponent<NavMeshAgent>();

            tree = new BTRoot();
            //Selector dayOrNight = new Selector("Day/Night");
            Sequence playerSequence = new Sequence("Player sequences");
            Leaf breadRestock = new Leaf("Restocking bread", BreadRestock);
            Leaf chickenRestock = new Leaf("Restocking chicken", ChickenRestock);
            Leaf saladRestock = new Leaf("Restocking salad", SaladRestock);
            Leaf tomatoRestock = new Leaf("Restocking tomato", TomatoRestock);
            Leaf chickenBurger = new Leaf("Restocking tomato", PrepareChickenBurger);
            Leaf chickenSalad = new Leaf("Restocking tomato", PrepareChickenSalad);
            Leaf tomatoSalad = new Leaf("Restocking tomato", PrepareTomatoSalad);
            Leaf night = new Leaf("Mimimimimimimi", Night);

            playerSequence.AddChild(night);
            playerSequence.AddChild(breadRestock);
            playerSequence.AddChild(chickenRestock);
            playerSequence.AddChild(saladRestock);
            playerSequence.AddChild(tomatoRestock);
            playerSequence.AddChild(chickenBurger);
            playerSequence.AddChild(chickenSalad);
            playerSequence.AddChild(tomatoSalad);
            tree.AddChild(playerSequence);

            tree.PrintTree();
        }

        private void Update()
        {
            treeStatus = tree.Process();
        }

        BTNode.Status PrepareChickenBurger()
        {
            if (rotate)
            {
                agent.SetDestination(waitPos.position);
                anim.SetBool("CanWalk", true);

                if (Vector3.Distance(agent.destination, transform.position) < 0.2f)
                {
                    anim.SetBool("CanWalk", false);
                    rotate = false;
                }

                return BTNode.Status.Success;
            }

            if (gm.chickenBurger && gm.day && gm.orderArrived)
            {
                if (checkIngredients)
                {
                    if (gm.currentBread < 2 || gm.currentChicken < 2)
                    {
                        return BTNode.Status.Success;
                    }
                    checkIngredients = false;
                }

                Debug.Log("burgero");

                if (canAddDish && !dishDelivered)
                {
                    agent.speed = 1.5f;
                    agent.SetDestination(workTable.position);
                    Quaternion lookRotation = Quaternion.LookRotation(workTable.position);
                    transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10);
                    anim.SetBool("CanWalk", true);
                }

                if (Vector3.Distance(agent.destination, transform.position) < 0.1f)
                {
                    anim.SetBool("CanWalk", false);

                    time += Time.deltaTime;

                    if (time <= 1.5f)
                    {
                        //agent.speed = 0;
                    }
                    else
                    {
                        agent.speed = 1.5f;

                        if (canAddDish)
                        {
                            gm.currentBread -= 2;
                            gm.currentChicken -= 2;
                            gm.currentChickenBurger += 1;
                            canAddDish = false;
                        }

                        agent.SetDestination(deliveryPos.position);
                        Quaternion lookRot = Quaternion.LookRotation(deliveryPos.position);
                        transform.rotation = Quaternion.Slerp(transform.rotation, lookRot, Time.deltaTime * 10);
                        anim.SetBool("CanWalk", true);

                        if (Vector3.Distance(agent.destination, transform.position) < 0.1f)
                        {
                            Debug.Log("Arrivato a consegna capo");

                            anim.SetBool("CanWalk", false);

                            //agent.speed = 0;

                            if (canDecreseDish)
                            {
                                gm.currentChickenBurger -= 1;
                                gm.orderText.text = "";
                                canDecreseDish = false;
                                dishDelivered = true;
                            }

                            
                            orderDelivered = true;
                            rotate = true;
                            gm.chickenBurger = false;
                            time = 0;
                            gm.orderArrived = false;
                            canAddDish = true;
                            canDecreseDish = true;
                            dishDelivered = false;
                            checkIngredients = true;

                            
                        }
                        
                    }
                    
                }

                

                return BTNode.Status.Failure;
            }
            return BTNode.Status.Success;
        }

        BTNode.Status PrepareChickenSalad()
        {

            /*if (checkIngredients)
            {
                if (gm.currentSalad < 3 || gm.currentChicken < 2)
                {
                    return BTNode.Status.Success;
                }
                checkIngredients = false;
            }
            */
            if (rotate)
            {
                agent.SetDestination(waitPos.position);
                anim.SetBool("CanWalk", true);

                if (Vector3.Distance(agent.destination, transform.position) < 0.2f)
                {
                    anim.SetBool("CanWalk", false);
                    rotate = false;
                }

                return BTNode.Status.Success;
            }

            if (gm.chickenSalad && gm.day && gm.orderArrived)
            {
                if (checkIngredients)
                {
                    if (gm.currentSalad < 3 || gm.currentChicken < 2)
                    {
                        return BTNode.Status.Success;
                    }
                    checkIngredients = false;
                }
                Debug.Log("saladccc");

                if (canAddDish && !dishDelivered)
                {
                    agent.speed = 1.5f;
                    agent.SetDestination(workTable.position);
                    Quaternion lookRotation = Quaternion.LookRotation(workTable.position);
                    transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10);
                    anim.SetBool("CanWalk", true);
                }

                if (Vector3.Distance(agent.destination, transform.position) < 0.1f)
                {
                    anim.SetBool("CanWalk", false);

                    time += Time.deltaTime;

                    if (time <= 1.5f)
                    {
                        //agent.speed = 0;
                    }
                    else
                    {
                        agent.speed = 1.5f;

                        if (canAddDish)
                        {
                            gm.currentSalad -= 3;
                            gm.currentChicken -= 2;
                            gm.currentChickenSalad += 1;
                            canAddDish = false;
                        }

                        agent.SetDestination(deliveryPos.position);
                        Quaternion lookRot = Quaternion.LookRotation(deliveryPos.position);
                        transform.rotation = Quaternion.Slerp(transform.rotation, lookRot, Time.deltaTime * 10);
                        anim.SetBool("CanWalk", true);

                        if (Vector3.Distance(agent.destination, transform.position) < 0.1f)
                        {
                            Debug.Log("Arrivato a consegna capo");

                            anim.SetBool("CanWalk", false);

                            //agent.speed = 0;

                            if (canDecreseDish)
                            {
                                gm.currentChickenSalad -= 1;
                                gm.orderText.text = "";
                                canDecreseDish = false;
                                dishDelivered = true;
                            }

                            time = 0;
                            orderDelivered = true;
                            gm.orderArrived = false;
                            canAddDish = true;
                            rotate = true;
                            canDecreseDish = true;
                            gm.chickenSalad = false;
                            dishDelivered = false;
                            checkIngredients = true;

                            
                        }
                    }
                }
                return BTNode.Status.Failure;
            }
            return BTNode.Status.Success;
        }

        BTNode.Status PrepareTomatoSalad()
        {

            /*  if (checkIngredients)
              { 
                  if (gm.currentSalad < 3 || gm.currentTomato < 2)
                  {
                      return BTNode.Status.Success;
                  }
                  checkIngredients = false;
              }
            */
            if (rotate)
            {
                agent.SetDestination(waitPos.position);
                anim.SetBool("CanWalk", true);

                if (Vector3.Distance(agent.destination, transform.position) < 0.2f)
                {
                    anim.SetBool("CanWalk", false);
                    rotate = false;
                }

                return BTNode.Status.Success;
            }

            if (gm.tomatoSalad && gm.day && gm.orderArrived)
            {
                if (checkIngredients)
                {
                    if (gm.currentSalad < 3 || gm.currentTomato < 2)
                    {
                        return BTNode.Status.Success;
                    }
                    checkIngredients = false;
                }
                Debug.Log("tomato");

                if (canAddDish && !dishDelivered)
                {
                    agent.speed = 1.5f;
                    agent.SetDestination(workTable.position);
                    Quaternion lookRotation = Quaternion.LookRotation(workTable.position);
                    transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10);
                    anim.SetBool("CanWalk", true);
                }

                if (Vector3.Distance(agent.destination, transform.position) < 0.1f)
                {
                    anim.SetBool("CanWalk", false);

                    time += Time.deltaTime;

                    if (time <= 1.5f)
                    {
                        //agent.speed = 0;
                    }
                    else
                    {
                        agent.speed = 1.5f;

                        if (canAddDish)
                        {
                            gm.currentSalad -= 3;
                            gm.currentTomato -= 2;
                            gm.currentTomatoSalad += 1;
                            canAddDish = false;
                        }

                        agent.SetDestination(deliveryPos.position);
                        Quaternion lookRot = Quaternion.LookRotation(deliveryPos.position);
                        transform.rotation = Quaternion.Slerp(transform.rotation, lookRot, Time.deltaTime * 10);
                        anim.SetBool("CanWalk", true);

                        if (Vector3.Distance(agent.destination, transform.position) < 0.1f)
                        {
                            Debug.Log("Arrivato a consegna capo");

                            anim.SetBool("CanWalk", false);

                            //agent.speed = 0;

                            if (canDecreseDish)
                            {
                                gm.currentTomatoSalad -= 1;
                                gm.orderText.text = "";
                                canDecreseDish = false;
                                dishDelivered = true;
                            }

                            time = 0;
                            orderDelivered = true;
                            rotate = true;
                            gm.orderArrived = false;
                            canAddDish = true;
                            canDecreseDish = true;
                            gm.tomatoSalad = false;
                            dishDelivered = false;
                            checkIngredients = true;

                            

                        }
                    }
                }

                return BTNode.Status.Failure;
            }
            return BTNode.Status.Success;
        }

        BTNode.Status BreadRestock()
        {
            if (!gm.day)
            {
                return BTNode.Status.Success;
            }

            if (gm.chickenBurger && gm.currentBread < 2 && gm.day) //&& gm.orderArrived)
            {
                Debug.Log("Prendere pane");

                agent.speed = 1.5f;
                agent.SetDestination(restockWaypoints[0].position);
                Quaternion lookRotation = Quaternion.LookRotation(restockTables[0].position);
                transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10);
                anim.SetBool("CanWalk", true);

                if (Vector3.Distance(agent.destination, transform.position) < 0.1f)
                {
                    anim.SetBool("CanWalk", false);
                    time += Time.deltaTime;

                    if (time <= 0.6f)
                    {
                        agent.speed = 0;
                    }
                    else
                    {
                        agent.speed = 1.5f;
                        gm.currentBread = 5;
                        time = 0; 
                        return BTNode.Status.Success;
                    }
                }

                return BTNode.Status.Failure;
            }
            return BTNode.Status.Success;
        }
       
        BTNode.Status ChickenRestock()
        {
            if (!gm.day)
            {
                return BTNode.Status.Success;
            }

            if ((gm.chickenBurger || gm.chickenSalad) && gm.currentChicken < 2 && gm.day) //&& gm.orderArrived)
            {
                Debug.Log("Prendere pollo");

                agent.speed = 1.5f;
                agent.SetDestination(restockWaypoints[1].position);
                Quaternion lookRotation = Quaternion.LookRotation(restockTables[1].position);
                transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10);
                anim.SetBool("CanWalk", true);

                if (Vector3.Distance(agent.destination, transform.position) < 0.1f)
                {
                    anim.SetBool("CanWalk", false);
                    time += Time.deltaTime;

                    if (time <= 0.6f)
                    {
                        agent.speed = 0;
                    }
                    else
                    {
                        gm.currentChicken = 5;
                        agent.speed = 1.5f;
                        time = 0;
                        return BTNode.Status.Success;
                    }
                }

                return BTNode.Status.Failure;
            }
            return BTNode.Status.Success;
        }
        
        BTNode.Status SaladRestock()
        {
            if (!gm.day)
            {
                return BTNode.Status.Success;
            }

            if ((gm.chickenSalad || gm.tomatoSalad) && gm.currentSalad < 3 && gm.day )//&& gm.orderArrived)
            {
                agent.speed = 1.5f;
                agent.SetDestination(restockWaypoints[2].position);
                Quaternion lookRotation = Quaternion.LookRotation(restockTables[2].position);
                transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10);
                anim.SetBool("CanWalk", true);

                if (Vector3.Distance(agent.destination, transform.position) < 0.1f)
                {
                    anim.SetBool("CanWalk", false);
                    time += Time.deltaTime;

                    if (time <= 0.6f)
                    {
                        agent.speed = 0;
                    }
                    else
                    {
                        gm.currentSalad = 5;
                        agent.speed = 1.5f;
                        time = 0;
                        return BTNode.Status.Success;
                    }
                }

                return BTNode.Status.Failure;
            }
            return BTNode.Status.Success;
        }
        BTNode.Status TomatoRestock()
        {
            if (!gm.day)
            {
                return BTNode.Status.Success;
            }

            if (gm.tomatoSalad && gm.currentTomato < 2 && gm.day) //&& gm.orderArrived )
            {
                agent.speed = 1.5f;
                agent.SetDestination(restockWaypoints[3].position);
                Quaternion lookRotation = Quaternion.LookRotation(restockTables[3].position);
                transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10);
                agent.updateRotation= true;
                anim.SetBool("CanWalk", true);

                if (Vector3.Distance(agent.destination, transform.position) < 0.1f)
                {
                    anim.SetBool("CanWalk", false);
                    time += Time.deltaTime;

                    if (time <= 0.6f)
                    {
                        agent.speed = 0;
                    }
                    else
                    {
                        gm.currentTomato = 5;
                        agent.speed = 1.5f;
                        time = 0;
                        return BTNode.Status.Success;
                    }
                }

                return BTNode.Status.Failure;
            }
            return BTNode.Status.Success;
        }

        BTNode.Status Night()
        {
            if (!gm.day && gm.waiUntilStartDay == 3)
            {
                if (!open)
                {
                    animPoke.SetBool("CanOpen", true);
                    animPoke.SetBool("CanClose", false);
                    open = true;
                }

                agent.speed = 1.5f;
                agent.SetDestination(bedPos.position);
                Quaternion lookRotation = Quaternion.LookRotation(agent.destination);
                transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10);
                anim.SetBool("CanWalk", true);

                if (Vector3.Distance(agent.destination, transform.position) < 0.1f)
                {
                    time += Time.deltaTime;

                    if (time >= 0.3f)
                    {
                        transform.GetChild(0).gameObject.SetActive(false);

                        if (time > 0.5f)
                        {
                            animPoke.SetBool("CanClose", true);
                            animPoke.SetBool("CanOpen", false);
                            time = 0;
                            //return BTNode.Status.Success;
                        }
                    }
                }

                if (gm.nightTime >= 9.2f)
                {
                    animPoke.SetBool("CanClose", false);
                    animPoke.SetBool("CanOpen", true);

                    wakeup= true;
                }

                return BTNode.Status.Failure;
            }
            else
            {
                if(wakeup)
                {
                    transform.GetChild(0).gameObject.SetActive(true);

                    agent.SetDestination(waitPos.position);
                    anim.SetBool("CanWalk", true);

                    if (Vector3.Distance(agent.destination, transform.position) < 0.2f)
                    {
                        animPoke.SetBool("CanClose", true);
                        animPoke.SetBool("CanOpen", false);

                        agent.speed = 0;

                        Debug.Log("fff");

                        anim.SetBool("CanWalk", false);

                        Quaternion lookToDelivery = Quaternion.Euler(deliveryPos.eulerAngles);
                        transform.rotation = Quaternion.Lerp(transform.rotation, lookToDelivery, Time.deltaTime * 18);

                        open = false;
                        animPoke.SetBool("NightFinished", true);

                        wakeup= false;
                    }
                    //return BTNode.Status.Failure;
                }

                return BTNode.Status.Success;
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;

            if(agent != null)
            Gizmos.DrawSphere(agent.destination, 0.2f);
        }

    }
}
